<?php

namespace Application\Sonata\UserBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Symfony\Component\Security\Http\HttpUtils;

class AuthenticationFailureHandler extends DefaultAuthenticationFailureHandler {

    protected $container;

    public function __construct(HttpKernelInterface $httpKernel, HttpUtils $httpUtils, array $options = array(), LoggerInterface $logger = null, $container) {
        parent::__construct($httpKernel, $httpUtils, $options, $logger);

        $this->httpKernel = $httpKernel;
        $this->httpUtils = $httpUtils;
        $this->options = $options;
        $this->logger = $logger;

        $this->container = $container;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
        // Redirigmos a donde haya que ir
        return parent::onAuthenticationFailure($request, $exception);
    }

}
