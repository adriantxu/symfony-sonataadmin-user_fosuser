<?php

// AppBundle\Form\Extension\FreeHtmlTypeExtension

namespace AppBundle\Form\Extension;

use AppBundle\Form\Type\FreeHtmlType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FreeHtmlTypeExtension extends AbstractTypeExtension {

    public function getExtendedType() {
        return FreeHtmlType::class;
    }

    public function configureOptions(OptionsResolver $resolver) {
        // Agregamos los campos extra
        $resolver->setDefined([
            'template',
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        // @link Ejemplo: https://symfony.com/doc/current/form/create_form_type_extension.html#adding-the-extension-business-logic
    }

}
