<?php

namespace AppBundle\DependencyInjection;

class SwiftMailerExtension {

    private $container;
    private $templating;

    public function __construct($container, $templating) {
        $this->container  = $container;
        $this->templating = $templating;
    }

    public function send($template, $title, $body, $to) {
        $baseDir = dirname(dirname(dirname(__DIR__)));

        $renderParams = [
            'title' => $title,
            'body'  => $body,
        ];

        return $this->sendBasicEmail($template, $baseDir, $title, $to, $renderParams);
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Prepara un correo basico con el titulo, cuerpo y destinatario y lo envia
     * 
     * @param string       $template
     * @param string       $baseDir
     * @param string       $title
     * @param string|array $to
     * @param array        $renderParams
     *
     * @return integer
     */
    private function sendBasicEmail($template, $baseDir, $title, $to, $renderParams) {
        $transport = new \Swift_SmtpTransport($this->container->getParameter('mailer_host'), $this->container->getParameter('mailer_port'), $this->container->getParameter('mailer_encryption'));
        $transport
                ->setUsername($this->container->getParameter('mailer_user'))
                ->setPassword($this->container->getParameter('mailer_password'))
        ;

        $mailer = new \Swift_Mailer($transport);

        $attachment = \Swift_Attachment::fromPath($baseDir . '/web/bundles/app/img/logo_title.png')->setFilename('logo')->setDisposition('inline');
        $attachment->getHeaders()->addTextHeader('Content-ID', '<logo>');
        $attachment->getHeaders()->addTextHeader('X-Attachment-Id', 'logo');

        $message = (new \Swift_Message($title))
                ->setFrom($this->container->getParameter('mailer_user'))
                ->setTo($to)
                ->attach($attachment)
                ->setBody(
                $this->templating->render(
                        $template, $renderParams
                ), 'text/html'
        );

        return $mailer->send($message);
    }

}
