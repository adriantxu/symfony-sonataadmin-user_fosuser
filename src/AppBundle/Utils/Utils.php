<?php

namespace AppBundle\Utils;

class Utils {

    /**
     * Devuelve el string de una latitud y longitud
     * 
     * Si no encuentra nada devuelve un string vacio
     * 
     * @param float  $lat    Latitud
     * @param float  $lng    Longitud
     * @param string $apiKey API_KEY de Google
     * 
     * @return string
     */
    public static function getGoogleMapLocationString($lat, $lng, $apiKey) {
        $location = '';

        // Hay que comprobar que vengan los parametros con un valor correcto
        foreach ([$lat, $lng] as $param) {
            if (in_array($param, [null, false, '', []])) {
                return $location;
            }
        }

        try {
            $geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $lng . '&key=' . $apiKey);
            $output             = json_decode($geocodeFromLatLong, true);

            if (is_array($output)) {
                // La posicion 1 parece tener la informacion mas clara
                $location = $output['results'][1]['formatted_address'];
            }
        } catch (\Exception $e) {
            $fileName = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'google_maps.log';

            $lineBreak = "\n";

            if (!is_file($fileName)) {
                file_put_contents($fileName, '');

                $lineBreak = '';
            }

            $fileData = file_get_contents($fileName);
            $fileData .= $lineBreak . '[' . date('Y-m-d H:i:s') . '] {"message": "' . $e->getMessage() . '"}, {"request_ip": "' . $_SERVER['REMOTE_ADDR'] . '"}';

            file_put_contents($fileName, $fileData);
        }


        return $location;
    }

    /**
     * Devuelve un listado de idiomas de la aplicacion
     * 
     * @return array
     */
    public static function getLanguages() {
        return [
            'es' => 'Castellano',
            'eu' => 'Euskera',
        ];
    }

    /**
     * Devuelve un texto que hay entre un texto de inicio y otro final
     * 
     * @param string $string
     * @param string $start
     * @param string $end
     * 
     * @return string|null
     */
    public static function getStrBetween($string, $start, $end) {
        $string = ' ' . $string;

        $ini = strpos($string, $start);

        if ($ini == 0) {
            return null;
        }

        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;

        return substr($string, $ini, $len);
    }

    /**
     * Devuelve el translator para poder traducir
     * Es obligatorio que el directorio y el archivo se llamen igual
     * 
     * @param string $domain    Opcional. Archivo de donde tiene que sacar las tracucciones
     * @param bool   $isFromApp Si viene a true hay que cargar el archivo de traducciones desde el directorio app
     * @param string $extension Extension del fichero. Por defecto yml
     * 
     * @return \Symfony\Component\Translation\Translator
     */
    public static function getTranslator($domain, $isFromApp = false, $extension = 'yml') {
        $resource = ('yml' === $extension) ? 'yaml' : $extension;

        $locale = (isset($_SESSION['_sf2_attributes']) && isset($_SESSION['_sf2_attributes']['_locale'])) ? $_SESSION['_sf2_attributes']['_locale'] : 'es';

        $translator = new \Symfony\Component\Translation\Translator($locale);

        $loader = '\Symfony\Component\Translation\Loader\\' . ucfirst($resource) . 'FileLoader';
        $translator->addLoader($resource, new $loader());

        if ($isFromApp) {
            $fileUrl = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . $domain . DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR . $domain . '.' . $locale . '.' . $extension;
        } else {
            $fileUrl = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR . lcfirst($domain) . DIRECTORY_SEPARATOR . $domain . '.' . $locale . '.' . $extension;
        }

        $translator->addResource($resource, $fileUrl, $locale);

        return $translator;
    }

    /**
     * Devuelve el nombre del mes de una fecha traducido
     * Es necesario que tenga el locale instalado en el servidor
     * 
     * @param \Datetime $date   Fecha de la que se quiere sacar el nombre del mes
     * @param string    $locale Locale al que se va a traducir
     * 
     * @return string
     */
    public static function getMonthNameTranslated($date, $locale = 'es_ES') {
        setlocale(LC_ALL, $locale);

        return ucfirst(strftime('%B', strtotime($date->format('Y-m-d H:i:s'))));
    }

    /**
     * Devuelve la URL del servidor
     * 
     * @return string
     */
    public function serverUrl() {
        $port = '';

        if (self::isDev()) {
            $port = ':' . $_SERVER['SERVER_PORT'];
        }

        $url = self::getProtocol() . $_SERVER['SERVER_NAME'] . $port;

        /*
          $parametersYml = __DIR__ . '/../../../app/config/parameters.yml';
          $values = Yaml::parse(file_get_contents($parametersYml));
          $values = $values['parameters'];

          if (Util::uploadToAmazon() && isset($values['amazon_s3_base_url'])) {
          $url = $values['amazon_s3_base_url'];
          }
         */

        return $url;
    }

    /**
     * Devuelve el protocolo de navegacion
     * 
     * @return string
     */
    public static function getProtocol() {
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') {
            return 'https://';
        } else {
            return 'http://';
        }
    }

    /**
     * Devuelve el icono del tiempo
     * 
     * @param string|int $codImg
     * 
     * @return string
     */
    public function imageWeather($codImg) {
        //$iconsWeather = 'http://www.aemet.es/imagenes_gcd/_iconos_municipios/' . intval($codImg) . '.png';
        $iconsWeather = self::serverUrl() . '/icons-weather/' . intval($codImg) . '.png';

        return $iconsWeather;
    }

    /**
     * Elimina los acentos de un string o array
     * 
     * @param string|array $str
     * 
     * @return string
     */
    public function removeAccents($str) {
        $a = ['À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή'];
        $b = ['A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η'];

        return str_replace($a, $b, $str);
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Comprueba si estamos en el entorno de desarrollo
     * 
     * @return boolean
     */
    private function isDev() {
        return '127.0.0.1' === $_SERVER['SERVER_NAME'];
    }

}
