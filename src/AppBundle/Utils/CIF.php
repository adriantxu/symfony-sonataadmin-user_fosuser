<?php

namespace AppBundle\Utils;

class CIF {

    /**
     * Validacin de un DNI
     * 
     * @param string $dni
     * 
     * @return boolean
     */
    public static function validarNIF($dni) {
        $fixedDocNumber = strtoupper($dni);

        return self::isValidNIF($fixedDocNumber) || self::isValidNIE($fixedDocNumber) || self::isValidCIF($fixedDocNumber);
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Comprueba sin un NIF es valido
     * 
     * @param string $docNumber
     * 
     * @return boolean
     */
    private function isValidNIF($docNumber) {
        $isValid        = false;
        $fixedDocNumber = '';

        $correctDigit = '';
        $writtenDigit = '';

        if (!preg_match('/^[A-Z]+$/i', substr($fixedDocNumber, 1, 1))) {
            $fixedDocNumber = strtoupper(substr('000000000' . $docNumber, -9));
        } else {
            $fixedDocNumber = strtoupper($docNumber);
        }

        $writtenDigit = strtoupper(substr($docNumber, -1, 1));

        if (self::isValidNIFFormat($fixedDocNumber)) {
            $correctDigit = self::getNIFCheckDigit($fixedDocNumber);

            if ($writtenDigit == $correctDigit) {
                $isValid = true;
            }
        }

        return $isValid;
    }

    /**
     * Comprueba si un NIE es valido
     * 
     * @param string $docNumber
     * 
     * @return boolean
     */
    private function isValidNIE($docNumber) {
        $isValid        = false;
        $fixedDocNumber = '';

        if (!preg_match('/^[A-Z]+$/i', substr($fixedDocNumber, 1, 1))) {
            $fixedDocNumber = strtoupper(substr('000000000' . $docNumber, -9));
        } else {
            $fixedDocNumber = strtoupper($docNumber);
        }

        if (self::isValidNIEFormat($fixedDocNumber)) {
            if ('T' === substr($fixedDocNumber, 1, 1)) {
                $isValid = true;
            } else {
                $numberWithoutLast = substr($fixedDocNumber, 0, strlen($fixedDocNumber) - 1);
                $lastDigit         = substr($fixedDocNumber, strlen($fixedDocNumber) - 1, strlen($fixedDocNumber));
                $numberWithoutLast = str_replace('Y', '1', $numberWithoutLast);
                $numberWithoutLast = str_replace('X', '0', $numberWithoutLast);
                $numberWithoutLast = str_replace('Z', '2', $numberWithoutLast);
                $fixedDocNumber    = $numberWithoutLast . $lastDigit;
                $isValid           = self::isValidNIF($fixedDocNumber);
            }
        }

        return $isValid;
    }

    /**
     * Comprueba si un CIF es valido
     * 
     * @param string $docNumber
     * 
     * @return boolean
     */
    private function isValidCIF($docNumber) {
        $isValid        = false;
        $fixedDocNumber = '';

        $correctDigit = '';
        $writtenDigit = '';

        $fixedDocNumber = strtoupper($docNumber);
        $writtenDigit   = substr($fixedDocNumber, -1, 1);

        if (self::isValidCIFFormat($fixedDocNumber) == 1) {
            $correctDigit = self::getCIFCheckDigit($fixedDocNumber);

            if ($writtenDigit == $correctDigit) {
                $isValid = true;
            }
        }

        return $isValid;
    }

    /**
     * Comprueba si el formato de un NIF es valido
     * 
     * @param string $docNumber
     * 
     * @return boolean
     */
    private function isValidNIFFormat($docNumber) {
        return self::respectsDocPattern($docNumber, '/^[KLM0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][a-zA-Z0-9]/');
    }

    /**
     * Comprueba si el formato de un NIE es valido
     * 
     * @param string $docNumber
     * 
     * @return boolean
     */
    private function isValidNIEFormat($docNumber) {
        return self::respectsDocPattern($docNumber, '/^[XYZT][0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Z0-9]/');
    }

    /**
     * Comprueba si el formato de un CIF es valido
     * 
     * @param string $docNumber
     * 
     * @return boolean
     */
    private function isValidCIFFormat($docNumber) {
        return self::respectsDocPattern($docNumber, '/^[PQSNWR][0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Z0-9]/') || self::respectsDocPattern($docNumber, '/^[ABCDEFGHJUV][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/');
    }

    /**
     * Devuelve la letra de un NIF
     * 
     * @param string $docNumber
     * 
     * @return string
     */
    private function getNIFCheckDigit($docNumber) {
        $keyString = 'TRWAGMYFPDXBNJZSQVHLCKE';

        $fixedDocNumber = '';

        $position      = 0;
        $writtenLetter = '';
        $correctLetter = '';

        if (!preg_match('/^[A-Z]+$/i', substr($fixedDocNumber, 1, 1))) {
            $fixedDocNumber = strtoupper(substr('000000000' . $docNumber, -9));
        } else {
            $fixedDocNumber = strtoupper($docNumber);
        }

        if (self::isValidNIFFormat($fixedDocNumber)) {
            $writtenLetter = substr($fixedDocNumber, -1);

            if (self::isValidNIFFormat($fixedDocNumber)) {
                $fixedDocNumber = str_replace('K', '0', $fixedDocNumber);
                $fixedDocNumber = str_replace('L', '0', $fixedDocNumber);
                $fixedDocNumber = str_replace('M', '0', $fixedDocNumber);

                $position      = substr($fixedDocNumber, 0, 8) % 23;
                $correctLetter = substr($keyString, $position, 1);
            }
        }

        return $correctLetter;
    }

    /**
     * Devuelve el numero de un CIF
     * 
     * @param string $docNumber
     * 
     * @return int
     */
    private function getCIFCheckDigit($docNumber) {
        $fixedDocNumber = '';

        $centralChars = '';
        $firstChar    = '';

        $evenSum           = 0;
        $oddSum            = 0;
        $totalSum          = 0;
        $lastDigitTotalSum = 0;

        $correctDigit = '';

        $fixedDocNumber = strtoupper($docNumber);

        if (self::isValidCIFFormat($fixedDocNumber)) {
            $firstChar    = substr($fixedDocNumber, 0, 1);
            $centralChars = substr($fixedDocNumber, 1, 7);

            $evenSum  = substr($centralChars, 1, 1) + substr($centralChars, 3, 1) + substr($centralChars, 5, 1);
            $oddSum   = self::sumDigits(substr($centralChars, 0, 1) * 2) + self::sumDigits(substr($centralChars, 2, 1) * 2) + self::sumDigits(substr($centralChars, 4, 1) * 2) + self::sumDigits(substr($centralChars, 6, 1) * 2);
            $totalSum = $evenSum + $oddSum;

            $lastDigitTotalSum = substr($totalSum, -1);

            if ($lastDigitTotalSum > 0) {
                $correctDigit = 10 - ( $lastDigitTotalSum % 10 );
            } else {
                $correctDigit = 0;
            }
        }

        // If CIF number starts with P, Q, S, N, W or R, check digit sould be a letter
        if (preg_match('/[PQSNWR]/', $firstChar)) {
            $correctDigit = substr('JABCDEFGHI', $correctDigit, 1);
        }

        return $correctDigit;
    }

    /**
     * @param string $givenString
     * @param string $pattern
     * 
     * @return boolean
     */
    private function respectsDocPattern($givenString, $pattern) {
        $isValid = false;

        $fixedString = strtoupper($givenString);

        if (is_int(substr($fixedString, 0, 1))) {
            $fixedString = substr('000000000' . $givenString, -9);
        }

        if (preg_match($pattern, $fixedString)) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Suma los digitos de un string
     * 
     * @param string $digits
     * 
     * @return int
     */
    private function sumDigits($digits) {
        $total = 0;
        $i     = 1;

        while ($i <= strlen($digits)) {
            $thisNumber = substr($digits, $i - 1, 1);
            $total      += $thisNumber;

            $i++;
        }

        return $total;
    }

    /**
     * Devuelve el texto de un tipo de identificacion
     * 
     * @param string $docNumber
     * 
     * @return string
     */
    private function getIdType($docNumber) {
        $identificationType = self::getIdentificationType();

        $docTypeDescription = '';
        $firstChar          = substr($docNumber, 0, 1);

        if (self::isValidNIFFormat($docNumber) || self::isValidNIEFormat($docNumber) || self::isValidCIFFormat($docNumber)) {
            $docTypeDescription = $identificationType[$firstChar];
        }

        return $docTypeDescription;
    }

    /**
     * Devuelve un listado de identificadores
     * 
     * @return array
     */
    private function getIdentificationType() {
        return [
            'K' => 'Español menor de catorce años o extranjero menor de dieciocho',
            'L' => 'Español mayor de catorce años residiendo en el extranjero',
            'M' => 'Extranjero mayor de dieciocho años sin NIE',
            '0' => 'Español con documento nacional de identidad',
            '1' => 'Español con documento nacional de identidad',
            '2' => 'Español con documento nacional de identidad',
            '3' => 'Español con documento nacional de identidad',
            '4' => 'Español con documento nacional de identidad',
            '5' => 'Español con documento nacional de identidad',
            '6' => 'Español con documento nacional de identidad',
            '7' => 'Español con documento nacional de identidad',
            '8' => 'Español con documento nacional de identidad',
            '9' => 'Español con documento nacional de identidad',
            'T' => 'Extranjero residente en España e identificado por la Policía con un NIE',
            'X' => 'Extranjero residente en España e identificado por la Policía con un NIE',
            'Y' => 'Extranjero residente en España e identificado por la Policía con un NIE',
            'Z' => 'Extranjero residente en España e identificado por la Policía con un NIE',
            /* As described in BOE number 49. February 26th, 2008 (article 3) */
            'A' => 'Sociedad Anónima',
            'B' => 'Sociedad de responsabilidad limitada',
            'C' => 'Sociedad colectiva',
            'D' => 'Sociedad comanditaria',
            'E' => 'Comunidad de bienes y herencias yacentes',
            'F' => 'Sociedad cooperativa',
            'G' => 'Asociación',
            'H' => 'Comunidad de propietarios en régimen de propiedad horizontal',
            'J' => 'Sociedad Civil => con o sin personalidad jurídica',
            'N' => 'Entidad extranjera',
            'P' => 'Corporación local',
            'Q' => 'Organismo público',
            'R' => 'Congregación o Institución Religiosa',
            'S' => 'Órgano de la Administración del Estado y Comunidades Autónomas',
            'U' => 'Unión Temporal de Empresas',
            'V' => 'Fondo de inversiones o de pensiones, agrupación de interés económico, etc',
            'W' => 'Establecimiento permanente de entidades no residentes en España'
        ];
    }

}
