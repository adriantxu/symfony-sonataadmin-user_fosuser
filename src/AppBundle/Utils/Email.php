<?php

namespace AppBundle\Utils;

class Email {

    /**
     * Comprueba que un email sea valido
     * 
     * @param string $email
     * 
     * @return bool
     */
    public static function verifyEmail($email): bool {
        return (1 === preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $email));
    }

}
