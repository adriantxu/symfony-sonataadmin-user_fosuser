<?php

namespace AppBundle\Utils;

use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

class Password {

    /**
     * Verificación de la password con BCryptPasswordEncoder
     * 
     * @param string $password
     * @param string $dbpassword
     * 
     * @return bool
     */
    public static function verifyPassword($password, $dbpassword) {
        $encoder = new BCryptPasswordEncoder(12);

        return ($encoder->isPasswordValid($dbpassword, $password, null));
    }

    /**
     * Codificación de la password con BCryptPasswordEncoder
     * 
     * @param string $plainPassword
     * 
     * @return bool
     */
    public static function encodePassword($plainPassword) {
        $encoder = new BCryptPasswordEncoder(12);

        return $encoder->encodePassword(trim($plainPassword), null);
    }

    /**
     * Verifica la longitud de un password
     * 
     * @param string $password
     * @param int    $length
     * 
     * @return bool
     */
    public static function verifyPasswordLength(string $password, int $length): bool {
        return strlen($password) >= $length;
    }

    /**
     * Crea una contraseña aleatoria
     * 
     * @param int $limit
     * 
     * @return string
     */
    public static function generateNewRandomPass($limit) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string     = '';

        for ($i = 0; $i < $limit; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

}
