<?php

namespace AppBundle\Utils;

class ArrayCollection {

    /**
     * Cuenta los elementos de un ArrayCollection que cumplan todos los criterios
     * Todos los criterios tienen que tener el valor pasado
     * 
     * @param \Doctrine\Common\Collections\ArrayCollection $items
     * @param array                                        $criteria
     *            propertyName => value
     * 
     * @return int
     */
    public static function count($items, $criteria) {
        $total = $items->count();

        if (!empty($criteria)) {
            $total = 0;

            foreach ($items as $item) {
                $passedCriteria = 0;

                foreach ($criteria as $propertyName => $value) {
                    $getMethod = 'get' . ucfirst($propertyName);

                    if (method_exists($item, $getMethod)) {
                        if ($item->$getMethod() === $value) {
                            $passedCriteria++;
                        }
                    }
                }

                if ($passedCriteria === count($criteria)) {
                    $total++;
                }
            }
        }

        return $total;
    }

    /**
     * Devuelve los elementos de un ArrayCollection que cumplan todos los criterios
     * Todos los criterios tienen que tener el valor pasado
     * 
     * @param \Doctrine\Common\Collections\ArrayCollection $items
     * @param array                                        $criteria
     *            propertyName => value
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public static function get($items, $criteria) {
        $result = clone($items);

        if (!empty($criteria)) {
            $result = new \Doctrine\Common\Collections\ArrayCollection();

            foreach ($items as $item) {
                $passedCriteria = 0;

                foreach ($criteria as $propertyName => $value) {
                    $getMethod = 'get' . ucfirst($propertyName);

                    if (method_exists($item, $getMethod)) {
                        if ($item->$getMethod() === $value) {
                            $passedCriteria++;
                        }
                    }
                }

                if ($passedCriteria === count($criteria)) {
                    $result[] = $item;
                }
            }
        }

        return $result;
    }

}
