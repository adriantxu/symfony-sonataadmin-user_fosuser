<?php

namespace AppBundle\Utils;

use AppBundle\Utils\ErrorCodes;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NotFoundExceptionListener {

    /**
     * Actualiza el event para asignar el error Not Found
     * 
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event) {
        if ($event->getException() instanceof NotFoundHttpException) {
            $translator = \AppBundle\Utils\Utils::getTranslator('messages');

            $messageNotFound = $translator->trans('errors.not_found');
            $messageExtra    = (empty($event->getException()->getMessage())) ? '' : ': ' . $event->getException()->getMessage();

            $error = [
                'success' => false,
                'error'   => [
                    'errorCode' => -1,
                    'errorDes'  => $messageNotFound . $messageExtra,
                    'errorUser' => $messageNotFound . $messageExtra,
                ]
            ];

            $response = new JsonResponse($error, ErrorCodes::NOT_EXIST_ENDPOINT);

            $event->setResponse($response);
        }
    }

}
