<?php

namespace AppBundle\Utils;

class Distance {

    /**
     * Calcula la distancia de 2 puntos
     * 
     * 	Units
     * 	    M => Millas
     * 	    K => Kilómetros
     * 	    N => Millas náuticas
     * 
     * @param float  $latOrigen
     * @param float  $lonOrigen
     * @param float  $latDestino
     * @param float  $lonDestino
     * @param string $unit
     * 
     * @return float
     */
    function calculateByCoord($latOrigen, $lonOrigen, $latDestino, $lonDestino, $unit = 'M') {
        $theta = $lonOrigen - $lonDestino;
        $dist  = sin(deg2rad($latOrigen)) * sin(deg2rad($latDestino)) + cos(deg2rad($latOrigen)) * cos(deg2rad($latDestino)) * cos(deg2rad($theta));
        $dist  = acos($dist);
        $dist  = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit  = strtoupper($unit);

        if ('K' === strtoupper($unit)) {
            return ($miles * 1.609344);
        } else if ('N' === strtoupper($unit)) {
            return ($miles * 0.8684);
        }

        return $miles;
    }

}
