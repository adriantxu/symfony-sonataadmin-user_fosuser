<?php

namespace AppBundle\Utils;

class ErrorCodes {

    const OK                 = 200;
    const MALFORMED          = 400;
    const NOT_AUTHENTICATED  = 401;
    const NOT_ACCESSIBLE     = 402;
    const FORBIDDEN          = 403;
    const NOT_EXIST_ENDPOINT = 404;
    const UNKNOWN_ERROR      = 500;

}
