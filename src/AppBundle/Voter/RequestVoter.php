<?php

namespace AppBundle\Voter;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Voter based on the uri
 */
class RequestVoter implements VoterInterface {

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * Checks whether an item is current.
     *
     * If the voter is not able to determine a result,
     * it should return null to let other voters do the job.
     *
     * @param ItemInterface $item
     * 
     * @return boolean|null
     */
    public function matchItem(ItemInterface $item) {
        $request = Request::createFromGlobals();

        if ($item->getUri() === $request->server->get('REQUEST_URI')) {
            return true;
        }

        return null;
    }

}
