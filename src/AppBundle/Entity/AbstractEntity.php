<?php

namespace AppBundle\Entity;

use AppBundle\Utils\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class AbstractEntity {

    protected $dbname;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Como el nombre va a ir en varias Entity lo agregamos aqui
     * 
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    protected $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    protected $deleted;

    public function __construct() {
        
    }

    public function __get($name) {
        return $this->$name;
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __toString() {
        if (method_exists($this, 'getName') && null !== $this->name) {
            return $this->getName();
        } else {
            return $this->getClassName();
        }
    }

    /**
     * Devuelve una Entity en array
     * 
     * @param array $fields      Opcional. Campos que se devuelven
     * @param array $extraFields Opcional. Campos extra que se devuelven
     * @param array $returnAs    Opcional. Si viene rellenado viene como clave-valor siendo la clave el nombre de la propiedad y el valor el nuevo nombre que se le va a dar
     * 
     * @return array
     */
    final public function toArray($fields = [], $extraFields = [], $returnAs = []) {
        $extraFields = array_change_key_case($extraFields, CASE_LOWER);
        $returnAs = array_change_key_case($returnAs, CASE_LOWER);

        $a = [];

        $reflection = new \ReflectionClass(get_class($this));
        foreach ($reflection->getProperties() as $reflectionProperty) {
            if ($reflection->getName() === $reflectionProperty->class) {
                // Si el campo fields viene rellenado y la propiedad actual no se encuentra en el listado, pasamos a la siguiente propiedad
                if (!empty($fields) && !in_array(strtolower($reflectionProperty->name), array_merge(array_map('mb_strtolower', $fields), array_keys($extraFields)))) {
                    continue;
                }

                $getMethod = 'get' . ucfirst($reflectionProperty->name);

                if (method_exists($this, $getMethod)) {
                    $property = $this->$getMethod();

                    // DateTime
                    if ($property && $property instanceof \DateTime) {
                        $property = $property->format('Y-m-d H:i:s');
                    }

                    // Entity
                    if ($property && (stristr($reflectionProperty->getDocComment(), '@ORM\ManyToOne') || (stristr($reflectionProperty->getDocComment(), '@ORM\OneToOne')))) {
                        // @todo Revisar que campos hay que devolver aqui
                        //$property = array_merge(['id' => $property->getId()], $property->getNameFields());
                        $property = ['id' => $property->getId()];
                    }

                    // ArrayCollection
                    if ($property && is_object($property) && in_array(get_class($property), ['Doctrine\ORM\PersistentCollection', 'Doctrine\Common\Collections\ArrayCollection'])) {
                        // Recuperamos los registros que no estan eliminados
                        $property = $this->$getMethod(['deleted' => null]);

                        $items = $property;

                        $property = [];

                        foreach ($items as $item) {
                            $fields = ['id' => $item->getId()];

                            $className = $item->getClassName();

                            if (array_key_exists(strtolower($className), $extraFields)) {
                                foreach ($extraFields[strtolower($className)] as $extraProperty) {
                                    $itemReflection = new \ReflectionClass(get_class($item));

                                    foreach ($itemReflection->getProperties() as $p) {
                                        if (strtolower($extraProperty) === strtolower($p->name)) {
                                            $pGetMethod = 'get' . ucfirst($p->name);

                                            $propertyName = (array_key_exists(strtolower($className), $returnAs) && is_array($returnAs[strtolower($className)]) && array_key_exists($p->name, $returnAs[strtolower($className)])) ? $returnAs[strtolower($className)][$p->name] : $p->name;

                                            $value = $item->$pGetMethod();
                                            $fields[$propertyName] = ($value instanceof \AppBundle\Entity\AbstractEntity) ? $value->getId() : $value;

                                            break;
                                        }
                                    }
                                }
                            }

                            $property[] = $fields;
                        }
                    }

                    $propertyName = (array_key_exists(strtolower($reflectionProperty->name), $returnAs) && !is_array($returnAs[strtolower($reflectionProperty->name)])) ? $returnAs[strtolower($reflectionProperty->name)] : $reflectionProperty->name;

                    $a[$propertyName] = $property;
                }
            }
        }

        if (!array_key_exists('id', $a)) {
            $a['id'] = $this->getId();
        }

        // Propiedades globales que tienen que ser agregadas manualmente
        foreach (['name', 'created', 'updated'] as $p) {
            if (empty($returnAs)) {
                $a[$p] = $this->getName();
            } else {
                if (array_key_exists($p, $returnAs)) {
                    $getMethod = 'get' . ucfirst($p);

                    $a[$returnAs[$p]] = $this->$getMethod();
                }
            }
        }

        return $a;
    }

    /**
     * Recoge los campos de un array y los convierte en una Entity
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param array                       $fields
     * @param array                       $fieldsAs Si el nombre de la propiedad se ha cambiado aqui viene la equivalencia ('title' => 'name')
     */
    final public function toEntity($em, $fields, $fieldsAs = []) {
        $reflectionClass = new \ReflectionClass($this);

        foreach ($fields as $key => $value) {
            $propertyName = $key;

            // Comprobamos si el nombre de la propiedad se ha cambiado y viene en el array $fieldsAs
            if (array_key_exists($key, $fieldsAs)) {
                $propertyName = $fieldsAs[$key];
            }

            $setMethod = 'set' . ucfirst($propertyName);
            $addMethod = 'add' . ucfirst(rtrim($propertyName, 's'));

            $property = $reflectionClass->getProperty($propertyName);

            if (method_exists($this, $setMethod)) {
                // Hay que comprobar el tipo de dato de la propiedad por si hay que hacer ajustes, como por ejemplo los DateTime
                $propertyType = Utils::getStrBetween($property->getDocComment(), 'type="', '"');

                // Si el tipo es null puede ser porque sea un OneToMany, ManyToOne o ManyToMany
                if (null === $propertyType) {
                    if (stristr($property->getDocComment(), '@ORM\ManyToOne')) {
                        $propertyType = 'ManyToOne';
                    }

                    if (stristr($property->getDocComment(), '@ORM\OneToOne')) {
                        $propertyType = 'OneToOne';
                    }
                }

                switch (strtolower($propertyType)) {
                    case 'datetime':
                        $fields[$key] = \DateTime::createFromFormat('Y-m-d H:i:s', $fields[$key]);

                        break;

                    case 'date':
                        $fields[$key] = \DateTime::createFromFormat('Y-m-d', $fields[$key]);

                        break;
                    case 'time':
                        $fields[$key] = \DateTime::createFromFormat('H:i:s', $fields[$key]);

                        break;

                    case 'onetoone':
                    case 'manytoone':
                        $classPath     = Utils::getStrBetween($property->getDocComment(), 'targetEntity="', '"');
                        $relatedEntity = new $classPath();
                        $classPath     = explode('\\', $classPath);

                        // Por si acaso el className esta entero (\AppBundle\Entity\Entityname) nos quedamos con el nombre de la clase y le concatenamos "AppBundle:"
                        $className = end($classPath);

                        $deleted = [];
                        if ($relatedEntity->hasColumn($em, 'deleted')) {
                            $deleted['deleted'] = null;
                        }

                        $fields[$key] = $em->getRepository('AppBundle:' . $className)->findOneBy(array_merge(['id' => $value], $deleted));

                        // Tiene que existir la Entity
                        if (empty($fields[$key])) {
                            $translator = Utils::getTranslator('messages');

                            $message = $translator->trans('errors.not_found_with_params', ['%ELEMENT%' => $className . ': ' . $value]);

                            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException($message);
                        }

                        break;
                }

                $this->$setMethod(isset($fields[$key]) ? $fields[$key] : null);
            } elseif (method_exists($this, $addMethod)) {
                // En este caso la propiedad se trata de un ArrayCollection y se utiliza el metodo addPROPERTY en vez de setPROPERTY
                $classPath = Utils::getStrBetween($property->getDocComment(), 'targetEntity="', '"');
                $classPathArr = explode('\\', Utils::getStrBetween($property->getDocComment(), 'targetEntity="', '"'));

                // Por si acaso el className esta entero (\AppBundle\Entity\Entityname) nos quedamos con el nombre de la clase y le concatenamos "AppBundle:"
                $className = end($classPathArr);

                foreach ($value as $item) {
                    if (isset($item['id'])) {
                        $entity = $em->getRepository('AppBundle:' . $className)->findOneBy(['id' => $item['id'], 'deleted' => null]);

                        // Tiene que existir la Entity
                        if (empty($entity)) {
                            $translator = Utils::getTranslator('messages');

                            $message = $translator->trans('errors.not_found_with_params', ['%ELEMENT%' => $className . ': ' . $item['id']]);

                            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException($message);
                        }
                    } else {
                        $entity = new $classPath();
                    }

                    $entity->toEntity($em, $item, $fieldsAs);

                    $this->$addMethod($entity);
                }
            }
        }
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * 
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @return \DateTime
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * @param \DateTime $deleted
     * 
     * @return $this
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Devuelve el nombre de la clase sin el namespace
     * 
     * @return string
     */
    final public function getClassName() {
        $path = explode('\\', get_class($this));

        return array_pop($path);
    }

    /**
     * Subida de ficheros
     * 
     * Para que funcione la ruta tiene que estar en una constante que se llame: NOMBREDELCAMPO_PATH
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param string|array                $fields
     */
    final public function uploadFile(\Doctrine\ORM\EntityManager $em, $fields) {
        if (!is_array($fields)) {
            $fields = [$fields];
        }

        $this->setDbname();

        $reflectionClass = new \ReflectionClass($this);

        foreach ($fields as $field) {
            $propertyName = ucfirst(strtolower($field));

            $getMethod = 'get' . $propertyName . 'File';
            $setMethod = 'set' . $propertyName;
            $constPath = strtoupper($field) . '_PATH';
            $newPath = constant(get_class($this) . '::' . $constPath);

            // El nuevo path tiene que empezar por /
            if ('/' !== substr($newPath, 0, strlen('/'))) {
                $newPath = '/' . $newPath;
            }

            // El nuevo path tiene que terminar por /
            if ('/' !== substr($newPath, -strlen('/'))) {
                $newPath = $newPath . '/';
            }

            // Recogemos el nombre de la columna que esta en el atributo name de la Propertyde la Entity
            $property = $reflectionClass->getProperty($field);
            $columnName = Utils::getStrBetween($property->getDocComment(), 'name="', '"');

            if ($this->$getMethod()) {
                $sql = 'SELECT
                            ' . $columnName . '
                        FROM
                            ' . $this->dbname . '
                        WHERE
                            id = ?;'
                ;

                $result = $em->getConnection()->fetchAssoc($sql, [$this->id]);

                // Recogemos el nuevo nombre del fichero
                $getMethod = 'get' . $propertyName . 'File';

                $fileName = $this->$getMethod()->getClientOriginalName();
                $extension = pathinfo($fileName, PATHINFO_EXTENSION);

                $isName = true;

                do {
                    $newName = md5(uniqid()) . '.' . $extension;

                    $sql = 'SELECT
                                ' . $columnName . '
                            FROM
                                ' . $this->dbname . '
                            WHERE
                                ' . $columnName . ' = ?;'
                    ;

                    $result = $em->getConnection()->fetchAssoc($sql, [$newName]);

                    $isName = (false !== $result);
                } while ($isName);

                $file = $this->$getMethod();

                $file->move($this->getDocumentRoot() . $newPath, $newName);

                // Comprobamos que la imagen se haya subido correctamente
                if (!is_file($this->getDocumentRoot() . $newPath . $newName)) {
                    throw new \Exception('Image not uploaded');
                }

                // Hay que comprobar si se guardan varios ficheros o un unico fichero
                $class = new \ReflectionClass($this);
                $properties = $class->getProperties();

                foreach ($properties as $p) {
                    if (strtolower($p->name) === strtolower($propertyName)) {
                        $property = $p;

                        break;
                    }
                }

                $docComment = ' ' . $property->getDocComment();

                if (false === strpos($docComment, 'type="array"')) {
                    $this->$setMethod($newPath . $newName);
                } else {
                    $newItem = [$newPath . $newName];

                    $pGetMethod = 'get' . $p->name;

                    $this->$setMethod(array_merge($this->$pGetMethod(), $newItem));
                }
            }
        }
    }

    /**
     * Eliminacion de uno o varios ficheros a una ruta en concreto
     * 
     * @param string|array $fields Campos para los que se suben las imagenes
     */
    final public function removeFile($fields) {
        if (!is_array($fields)) {
            $fields = [$fields];
        }

        foreach ($fields as $field) {
            $getMethod = 'get' . ucfirst(strtolower($field));
            $setMethod = 'set' . ucfirst(strtolower($field));

            if (method_exists($this, $getMethod)) {
                $files = $this->$getMethod();

                if (!is_array($files)) {
                    $files = [$files];
                }

                foreach ($files as $file) {
                    if (!empty($file) && is_file($_SERVER['DOCUMENT_ROOT'] . $file)) {
                        unlink($_SERVER['DOCUMENT_ROOT'] . $file);
                    }
                }

                // Cuando se eliminan todos los ficheros actualizamos el campo y lo ponemos vacio
                if (is_array($files)) {
                    $this->$setMethod([]);
                } else {
                    $this->$setMethod('');
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                 EVENTS                                 //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * @ORM\PrePersist
     */
    public function setCreatedValue() {
        $this->created = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedValue() {
        $this->updated = new \DateTime();
    }

    /**
     * Devuelve un campo booleano traducido
     * 
     * @param int|boolean $value
     * 
     * @return string
     */
    final protected function getBooleanTrans($value) {
        $translator = Utils::getTranslator('SonataAdminBundle', true, 'xliff');

        if ($value) {
            return $translator->trans('label_type_yes');
        } else {
            return $translator->trans('label_type_no');
        }
    }

    /**
     * Sacamos el nombre de la tabla
     */
    protected function setDbname() {
        $class = new \ReflectionClass($this);

        // Eliminamos los saltos de linea, espacios y el asterisco que pudiera haber si la documentacion estuviera en varias lineas
        $docComment = ' ' . str_replace(['\r', '\n', ' '], '', $class->getDocComment());
        $docComment = str_replace('Table(*name=', 'Table(name=', $docComment);

        $this->dbname = Utils::getStrBetween($docComment, '@ORM\Table(name="', '"');
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Devuelve la ruta hacia la carpeta web
     * 
     * @return string
     */
    private function getDocumentRoot() {
        return __DIR__ . '/../../../web';
    }

}
