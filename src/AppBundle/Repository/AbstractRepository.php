<?php

namespace AppBundle\Repository;

class AbstractRepository extends \Doctrine\ORM\EntityRepository {

    /**
     * Devuelve el nombre de la tabla
     * 
     * @return string
     */
    final protected function getSqlTable() {
        return $this->_class->table['name'];
    }

}
