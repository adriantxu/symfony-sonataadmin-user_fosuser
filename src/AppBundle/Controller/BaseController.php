<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends Controller {

    protected $translationDomain = 'messages';

    /**
     * Idioma por defecto
     *
     * @var string
     */
    protected $lang = 'es';

    /**
     * Respuesta por defecto
     *
     * @var array
     */
    protected $baseResponse = [
        'success' => false,
        'message' => 'UNAUTHORIZED'
    ];

    /**
     * Estado por defecto
     *
     * @var integer
     */
    protected $baseStatus = 401;

    /**
     * Actualiza el locale de la aplicacion
     * 
     * @Route("set_locale", name="set_locale", methods={"POST"})
     */
    public function setLocale(Request $request) {
        if ($request->isXmlHttpRequest()) {
            try {
                $lang = $request->request->get('lang');

                if (!$lang) {
                    throw new \Exception($this->translate($request, 'errors.mandatory_param', ['%PARAMETRO%' => 'lang'], 'Api'));
                }

                if (!in_array($lang, array_flip(\AppBundle\Utils\Utils::getLanguages()))) {
                    throw new \Exception($this->translate($request, 'errors.language_not_found', [], 'SonataAdminBundle'));
                }

                $request->getSession()->set('_locale', $lang);

                $this->setSuccessResponse();
            } catch (\Exception $e) {
                $this->setErrorResponse($e->getMessage());
            }
        }

        return new \Symfony\Component\HttpFoundation\JsonResponse($this->baseResponse, $this->baseStatus);
    }

    ////////////////////////////////////////////////////////////////////////////

    protected function setSuccessResponse($message = null, $data = null) {
        $this->baseResponse = [
            'success' => true,
            'message' => $message
        ];

        if (null !== $data) {
            foreach ($data as $key => $value) {
                $this->baseResponse['data'][$key] = $value;
            }
        }

        $this->baseStatus = 200;
    }

    protected function setErrorResponse($message = null, $data = null, $status = 200) {
        $this->baseResponse = [
            'success' => false,
            'message' => $message
        ];

        if (null !== $data) {
            foreach ($data as $key => $value) {
                $this->baseResponse['data'][$key] = $value;
            }
        }

        $this->baseStatus = $status;
    }

    /**
     * Comprobamos los campos lang y token (si es necesario) en la cabecera
     * 
     * @param Request $request
     * @param boolean $checkToken
     * 
     * @return boolean
     */
    protected function checkAccess($request, $checkToken = true) {
        $requiredHeaders = [
            'lang'
        ];

        if ($checkToken) {
            $requiredHeaders[] = 'token';
        }

        $result = true;

        foreach ($requiredHeaders as $requiredHeader) {
            if (!isset($this->getAllHeaders()[$requiredHeader])) {
                $this->setErrorResponse($this->translate($request, 'errors.mandatory_param', ['%PARAMETRO%' => $requiredHeader]));

                $result = false;

                break;
            }
        }

        // Actualizamos el idioma
        $this->lang = (isset($this->getAllHeaders()['lang'])) ? $this->getAllHeaders()['lang'] : $this->lang;

        return $result;
    }

    /**
     * Comprueba si vienen los parametros USERNAME y PASSWORD y si existe un usuario en la base de datos
     * 
     * @param Request $request
     * 
     * @return boolean|\Application\Sonata\UserBundle\Entity\User
     */
    protected function checkUserLogin(Request $request) {
        $result = false;

        if (!$username = $request->request->get('username')) {
            $this->setErrorResponse($this->setErrorResponse($this->translate($request, 'errors.mandatory_param', ['%PARAMETRO%' => 'username'])));
        }

        if ($username && !$password = $request->request->get('password')) {
            $this->setErrorResponse($this->setErrorResponse($this->translate($request, 'errors.mandatory_param', ['%PARAMETRO%' => 'password'])));
        }

        if ($username && $password) {
            try {
                $user = $this->get('fos_user.user_manager')->findUserByUsername($username);

                // Si no existe el usuario sacamos un error
                if (!$user instanceof \Application\Sonata\UserBundle\Entity\User) {
                    throw new \Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
                }

                // Comprobamos que el usuario haya introducido bien el username y la contraseña
                if (false === $this->get('security.password_encoder')->isPasswordValid($user, $password)) {
                    throw new \Symfony\Component\Security\Core\Exception\BadCredentialsException();
                } else {
                    // Comprobamos que el usuario este activo
                    if (true !== $user->isEnabled()) {
                        throw new \Symfony\Component\Security\Core\Exception\DisabledException();
                    } else {
                        $result = $user;
                    }
                }
            } catch (\Symfony\Component\Security\Core\Exception\UsernameNotFoundException $e) {
                $this->setErrorResponse($this->translate($request, 'errors.login.not_found_user'));
            } catch (\Symfony\Component\Security\Core\Exception\DisabledException $e) {
                $this->setErrorResponse($this->translate($request, 'errors.login.account_disabled'));
            } catch (\Symfony\Component\Security\Core\Exception\BadCredentialsException $e) {
                $this->setErrorResponse($this->translate($request, 'errors.login.incorrect_user_password'));
            } catch (\Exception $e) {
                // Para cualquier otra excepcion devolvemos false
                $this->setErrorResponse('Unknown error');
            }
        }

        return $result;
    }

    /**
     * Comprueba si vienen los TOKEN en la cabecera
     * 
     * @param Request $request
     * 
     * @return boolean|\Application\Sonata\UserBundle\Entity\User
     */
    protected function checkUserToken(Request $request) {
        $result = false;

        $token = (isset($this->getAllHeaders()['token'])) ? $this->getAllHeaders()['token'] : null;

        if (!$token) {
            $this->setErrorResponse($this->translate($request, 'errors.mandatory_param', ['%PARAMETRO%' => 'token']));
        } else {
            try {
                $user = $this->get('fos_user.user_manager')->findUserBy(['token' => $token]);

                // Si no existe el usuario sacamos un error
                if (!$user instanceof \Application\Sonata\UserBundle\Entity\User) {
                    throw new \Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
                }

                // Comprobamos que el usuario este activo
                if (true !== $user->isEnabled()) {
                    throw new \Symfony\Component\Security\Core\Exception\DisabledException();
                }

                $result = $user;
            } catch (\Symfony\Component\Security\Core\Exception\UsernameNotFoundException $e) {
                $this->setErrorResponse($this->translate($request, 'errors.login.not_found_user'));
            } catch (\Symfony\Component\Security\Core\Exception\DisabledException $e) {
                $this->setErrorResponse($this->translate($request, 'errors.login.account_disabled'));
            } catch (\Exception $e) {
                // Para cualquier otra excepcion devolvemos false
                $this->setErrorResponse($this->translate($request, 'errors.unknown_error'));
            }
        }

        return $result;
    }

    /**
     * Deslogue del usuario
     * 
     * @param \Application\Sonata\UserBundle\Entity\User $user
     */
    protected function logoutUser($user) {
        $user->setToken(null);

        $this->get('fos_user.user_manager')->updateUser($user);
    }

    /**
     * Traduce un texto desde los archivos de traduccion en un idioma en concreto
     * 
     * @param Request $request
     * @param string  $message
     * @param array   $params
     * 
     * @return string
     */
    protected function translate($request, $message, $params = [], $translationDomain = null) {
        if (!$translationDomain) {
            $translationDomain = $this->translationDomain;
        }

        $lang = $request->request->get('lang');
        $lang = ($lang) ?: 'es';

        return $this->get('translator')->trans($message, $params, $translationDomain, $lang);
    }

    /**
     * Devuelve las cabeceras
     * 
     * @return array
     */
    protected function getAllHeaders() {
        if (function_exists('getallheaders')) {
            return getallheaders();
        } else {
            $headers = [];

            foreach ($_SERVER as $name => $value) {
                if (substr($name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                }
            }

            return $headers;
        }
    }

}
